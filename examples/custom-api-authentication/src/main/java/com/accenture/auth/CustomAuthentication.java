package com.accenture.auth;

import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.EventEnvelope;
import org.platformlambda.core.models.LambdaFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CustomAuthentication implements LambdaFunction {
    private static final Logger log = LoggerFactory.getLogger(CustomAuthentication.class);

    private static final String DUMMY_ACCESS_TOKEN = "helloworld";

    @Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws Exception {
        AsyncHttpRequest request = new AsyncHttpRequest(body);
        if (request.getMethod() == null) {
            log.error("Input is not a HTTP request");
            return false;
        }
        log.info("{}", request.toMap());
        log.info("query {} - {}", request.getQueryParameter("token"), DUMMY_ACCESS_TOKEN.equals(request.getQueryParameter("token")));

        /*
         * Sample code to demonstrate checking a dummy access token.
         *
         * You should implement your own authentication logic here
         */
        String method = request.getMethod();
        if ("GET".equals(method)) {
            if (DUMMY_ACCESS_TOKEN.equals(request.getQueryParameter("token"))) {
                //
                // If you set header(s) in the response event envelope, the headers will be propagated
                // as HTTP request headers if the target service is an HTTP endpoint.
                //
                // If the target service is a lambda function, the headers will be sent delivered as
                // "sessionInfo" in the AsyncHttpRequest object.
                //
                return new EventEnvelope().setHeader("X-User-Profile", "demo-user").setBody(true);
            } else {
                return false;
            }
        } else if ("POST".equals(method)) {
            if (DUMMY_ACCESS_TOKEN.equals(request.getHeader("X-Token"))) {
                return new EventEnvelope().setHeader("X-User-Profile", "demo-user").setBody(true);
            } else {
                return false;
            }
        } else {
            // Of course, you can also throw exception to reject the request
            // Note that AppException allows you to set HTTP status.
            // IllegalArgumentException is translated as HTTP-400.
            throw new IllegalArgumentException("Not implemented");
        }
    }
}
