package com.accenture.demo;

import org.platformlambda.core.exception.AppException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

@Path("/hello")
public class DemoEndpoint {

    @GET
    @Path("/world")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_HTML})
    public Map<String, Object> hello(@Context HttpServletRequest request) throws IOException, TimeoutException, AppException {

        //////////////////////////////////////////////////////////////////
        // for testing only - you don't need this endpoint for production
        //////////////////////////////////////////////////////////////////

        Map<String, Object> forward = new HashMap<>();
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String key = headers.nextElement();
            forward.put(key, request.getHeader(key));
        }
        Map<String, Object> result = new HashMap<>();
        result.put("headers", forward);
        result.put("demo_timestamp", new Date());
        return result;
    }
}
