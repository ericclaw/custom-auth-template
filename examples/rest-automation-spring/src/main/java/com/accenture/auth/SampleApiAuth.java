package com.accenture.auth;

import org.platformlambda.core.exception.AppException;
import org.platformlambda.core.models.AsyncHttpRequest;
import org.platformlambda.core.models.LambdaFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SampleApiAuth implements LambdaFunction {
    private static final Logger log = LoggerFactory.getLogger(SampleApiAuth.class);

    @Override
    public Object handleEvent(Map<String, String> headers, Object body, int instance) throws AppException {
        AsyncHttpRequest request = new AsyncHttpRequest(body);
        if (request.getMethod() == null) {
            throw new AppException(400, "Input is not a HTTP request");
        }
        log.info("API authentication service got: {} {}", request.getMethod(), request.getUrl());
        // OK - this is just a demo. Returning 'true' means approval.
        return true;
    }
}
